import numpy as np
import matplotlib.pyplot as plt
import argparse
import glob
import pickle

parser = argparse.ArgumentParser()
parser.add_argument('--run', type=int, nargs="+",default = 0, required=True)
parser.add_argument('--channels',type=int,nargs="+",default=1)
parser.add_argument('--time_range',type=float,nargs="+",default=[4,6])
# parser.add_argument('--path',type=str,default="out",required=False)

args = parser.parse_args()

overlay_runs = False
run = args.run
if len(run)>1:
    overlay_runs=True
    second_run=int(run[1])

run=int(run[0])

channels = args.channels
time_range = args.time_range
print(channels)

file_list = sorted(glob.glob("data/csv/wave{}*.csv".format(run)))
print("Converting Run {}.".format(run))
print("Found {} channels, in files {}.".format(len(file_list),file_list)) 

waves = []
for chan in channels:
    file = "data/csv/wave{}_{}.csv".format(run,chan)
    # if i==1 or i==2 or i==3: continue
    if file not in file_list: 
        print("Error: channel {} was requested but not found.".format(chan))
    channel = np.genfromtxt(file, delimiter=',',dtype=np.double)
    print(type(channel))
    channel[:,0]*=1000. ## convert to ms
    channel[:,1]*=1000. ## convert to mV
    waves.append(channel)

print(waves[0][1000:1020,0]) 
if overlay_runs:
    waves2 =[]
    for chan in channels:
        file = "data/csv/wave{}_{}.csv".format(second_run,chan)

        channel = np.genfromtxt(file, delimiter=',')
        channel[:,0]*=1000. ## convert to ms
        channel[:,1]*=1000. ## convert to mV
        waves2.append(channel)


disp, ax = plt.subplots(1,1,figsize=(8,4.7), dpi=150,tight_layout = True)

ax.plot(waves[0][:,0],waves[0][:,1], 'orange', marker = ".", alpha = 1,markersize =1 )
ax.plot(waves[1][:,0],waves[1][:,1], 'blue', marker = ".", alpha = 1,markersize =1 )
ax.plot(waves[2][:,0],waves[2][:,1], 'red', marker = ".", alpha = 1,markersize =1 )
ax.plot(waves[3][:,0],waves[3][:,1], 'green', marker = ".", alpha = 1,markersize =1 )
if overlay_runs:
    ax.plot(waves2[0][:,0],waves2[0][:,1], 'blue', marker = ".", alpha = 1,markersize =1 )

# ax.plot(waves[1][:,0],waves[1][:,1], 'blue', marker = ".", alpha = 0.5,markersize =1 )
# plt.title("Event {}".format(n))
ax.set_xlabel('Time [ms]')
ax.set_ylabel('Voltage [mV]')
disp.savefig("displays/event{}.png".format(run))

### convert from time range parameter in ms to sample index.
# low_time_index = np.where(waves[0][:,0] == time_range[0])[0][0]
low_time_index = np.where( abs(waves[0][:,0] - time_range[0]) < 1e-6)[0][0]
# high_time_index = np.where(waves[0][:,0] == time_range[1])[0][0]
high_time_index = np.where( abs(waves[0][:,0] - time_range[1]) < 1e-6)[0][0]


disp2, ax2 = plt.subplots(1,1,figsize=(8,4.7), dpi=150,tight_layout = True)

time_unit = "ms"
if time_range[1] - time_range[0] < 0.5: time_unit = "us"
if time_range[1] - time_range[0] < 0.0005: time_unit = "ns"

factor=1.
if time_unit == "us": factor=1000.
if time_unit == "ns": factor=1000000.

# ax2.plot(waves[0][low_time_index:high_time_index,1], 'orange', marker = ".", alpha = 1,markersize =3 )
ax2.plot(factor*waves[0][low_time_index:high_time_index,0],waves[0][low_time_index:high_time_index,1], 'orange', marker = ".", alpha = 1,markersize =3 )
ax2.plot(factor*waves[1][low_time_index:high_time_index,0],waves[1][low_time_index:high_time_index,1], 'blue', marker = ".", alpha = 1,markersize =3 )
ax2.plot(factor*waves[2][low_time_index:high_time_index,0],waves[2][low_time_index:high_time_index,1], 'red', marker = ".", alpha = 1,markersize =3 )
ax2.plot(factor*waves[3][low_time_index:high_time_index,0],waves[3][low_time_index:high_time_index,1], 'green', marker = ".", alpha = 1,markersize =3 )
if overlay_runs:
    ax2.plot(factor*waves2[0][low_time_index:high_time_index,0],waves2[0][low_time_index:high_time_index,1], 'blue', marker = ".", alpha = 1,markersize =3 )
    # ax2.plot(waves2[0][low_time_index:high_time_index,1], 'blue', marker = ".", alpha = 1,markersize =3 )

# ax.plot(waves[1][:,0],waves[1][:,1], 'blue', marker = ".", alpha = 0.5,markersize =1 )
ax2.set_title("Event {}, {} ms".format(run,time_range[0]))
ax2.set_xlabel('Time [{}]'.format(time_unit))
ax2.set_ylabel('Voltage [mV]')
disp2.savefig("displays/event{}_zoom{}to{}ms.png".format(run,time_range[0],time_range[1]))


# p_energy_vs_npix = axs5[1].scatter([c["energy"] for c in processed_clusters] ,[ c["npix"] for c in processed_clusters],s=2)
# axs5[1].set_xlabel('Cluster energy [keV]')
# axs5[1].set_ylabel('Number of pixels')
# axs5[1].set_title('All clusters')
# axs5[1].set_xlim(0,400)
# axs5[1].set_ylim(0,200)

disp3, ax3 = plt.subplots(1,1,figsize=(8,4.7), dpi=150,tight_layout = True)

ax3.plot(waves[0][low_time_index:high_time_index,0],waves[0][low_time_index:high_time_index,1], 'orange', marker = ".", alpha = 1,markersize =3 )
ax3.scatter(waves[0][low_time_index:high_time_index,0],waves[0][low_time_index:high_time_index,1],s=2)
# if overlay_runs:
#     ax3.plot(waves2[0][low_time_index:high_time_index,0],waves2[0][low_time_index:high_time_index,1], 'blue', marker = ".", alpha = 1,markersize =3 )

# ax.plot(waves[1][:,0],waves[1][:,1], 'blue', marker = ".", alpha = 0.5,markersize =1 )
ax3.set_title("Event {}, {} ms".format(run,time_range[0]))
ax3.set_xlabel('Time [{}]'.format(time_unit))
ax3.set_ylabel('Voltage [mV]')
disp3.savefig("displays/event{}_zoom{}to{}ms_alt.png".format(run,time_range[0],time_range[1]))