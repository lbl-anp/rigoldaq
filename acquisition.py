import numpy as np
import sys
import optparse
import argparse
import signal
import os
import time
import datetime
import pyvisa as visa
import glob
import json

saveData = True
parser = argparse.ArgumentParser()
parser.add_argument('--note', type=str, default = "", required=False)
# parser.add_argument('--path',type=str,default="out",required=False)

args = parser.parse_args()
note = args.note
## timebase, seconds / div
timebase = 20.0E-3
horizontal_delay = 4.5 ## divisions

## vertical scale in volts / div. 0 disables channel
#### NB must enable channels for SAVING manually on scope. Storage -> Save Wave -> Channels and toggle on.
scale_ch1 = 0.05
scale_ch2 = 0.07
scale_ch3 = 0.07
scale_ch4 = 0.07
## offsets in volts
offset_ch1 = -0.150
offset_ch2 = -0.150
offset_ch3 = -0.150
offset_ch4 = -0.150

##trigger config
trigger_channel = "CHAN1"
trigger_threshold = 0.050 ##volts

def get_next_run_number():
    run_num_file = "next_run_number.txt"
    FileHandle = open(run_num_file)
    nextNumber = int(FileHandle.read().strip())
    FileHandle.close()
    FileHandle = open(run_num_file,"w")
    FileHandle.write(str(nextNumber+1)+"\n") 
    FileHandle.close()
    return nextNumber


run_num = get_next_run_number()
now = datetime.datetime.now()
current_time = now.strftime("%H:%M:%S")
print("\n\n----------------------------------------------------------------------")
print("######### Preparing for Run {}. Current time {} #########\n".format(run_num,current_time))


"""#################SEARCH/CONNECT#################"""
# establish communication with scope
initial = time.time()
# rm = pyvisa.ResourceManager("@py")
rm = visa.ResourceManager()
rigol = rm.open_resource('TCPIP::169.254.118.80::INSTR')
rigol.timeout = 3000000
rigol.encoding = 'latin_1'
rigol.clear()

print(rigol.query('*idn?'))
rigol.write(":CLEAR") ### seems required for acquisition parameters to be set.
active_channels = []
if scale_ch1!=0:
    rigol.write(':CHANnel1:DISPlay ON')
    rigol.write(':CHANnel1:SCALe {}'.format(scale_ch1))
    rigol.write(':CHANnel1:OFFSet {}'.format(offset_ch1))
    active_channels.append(1)
else:
    rigol.write(':CHANnel1:DISPlay OFF')

if scale_ch2!=0:
    rigol.write(':CHANnel2:DISPlay ON')
    rigol.write(':CHANnel2:SCALe {}'.format(scale_ch2))
    rigol.write(':CHANnel2:OFFSet {}'.format(offset_ch2))
    active_channels.append(2)

else:
    rigol.write(':CHANnel2:DISPlay OFF')

if scale_ch3!=0:
    rigol.write(':CHANnel3:DISPlay ON')
    rigol.write(':CHANnel3:SCALe {}'.format(scale_ch3))
    rigol.write(':CHANnel3:OFFSet {}'.format(offset_ch3))
    active_channels.append(3)

else:
    rigol.write(':CHANnel3:DISPlay OFF')

if scale_ch4!=0:
    rigol.write(':CHANnel4:DISPlay ON')
    rigol.write(':CHANnel4:SCALe {}'.format(scale_ch4))
    rigol.write(':CHANnel4:OFFSet {}'.format(offset_ch4))
    active_channels.append(4)

else:
    rigol.write(':CHANnel4:DISPlay OFF')
### these commands don't work, needs to be set manually ??
# rigol.write(':SAVE:CSV:CHANnel CHANnel1,1')
# rigol.write(':SAVE:CSV:CHANnel CHANnel2,1')
# rigol.write(':SAVE:CSV:CHANnel CHANnel3,1')
# rigol.write(':SAVE:CSV:CHANnel CHANnel4,1')

# rigol.write(':ACQuire:MDEPth 50M')
# rigol.write(':ACQuire:SRATe 1.0E-9')
rigol.write(':TIMebase:SCALe {}'.format(timebase))
rigol.write(':TIMebase:HREFerence:MODE TRIG') ### After acquisition, if you turn the timebase knob, it will expand/compress holding the trigger position constant
# print(rigol.query(':TIMebase:MAIN:OFFSet?')) ###
absolute_delay = timebase * horizontal_delay
rigol.write(':TIMebase:MAIN:OFFSet {}'.format(absolute_delay)) ###

rigol.write(':TRIGger:MODE Edge')
rigol.write(':TRIGger:EDGE:SOURce {}'.format(trigger_channel))
rigol.write(':TRIGger:EDGE:LEVEL {}'.format(trigger_threshold))



# print("Sampling rate: ",rigol.query(':ACQuire:SRATe?'))
# print("Memory depth: ",rigol.query(':ACQuire:MDEPth?')`)
# print("Time per div: ",rigol.query(':TIMebase:SCALe?'))
rigol.write(":CLEAR")
print("Arming trigger.")
rigol.write('*CLS;:SINGle')

while True:
    time.sleep(1)
    status = rigol.query(":TRIGger:STATus?")
    # print(status)
    if "WAIT" not in status: break
    else: 
        print("Waiting...")

print("Trigger fired.\n")
sample_rate = float(rigol.query(':ACQuire:SRATe?'))/1e9# GSa/s
horizontal_scale = float(rigol.query(':TIMebase:SCALe?'))*1000. #ms per div
memory_depth = float(rigol.query(':ACQuire:MDEPth?'))/1e6 ## MSa

print("Run {} used sampling rate {} GSa/s, time per division: {} ms, total samples per chan: {:.1f} MSa. \n".format(run_num,sample_rate, horizontal_scale,memory_depth))
# print("Memory depth: ",rigol.query(':ACQuire:MDEPth?'))
# print("Time per div: ",rigol.query(':TIMebase:SCALe?'))
# print("OPC",rigol.query("*OPC?"))
# filename = "bin\\wave{}".format(run_num)
filename = "wave{}".format(run_num)
if saveData:
    print("Saving Run {} to disk.".format(run_num))
    rigol.write(':SAVE:WAVeform D:\{}'.format(filename))
filename += ".bin"
filename = "bin\\"+filename
print(filename)

run_info_dict = {}
run_info_dict["note"] = note
run_info_dict["run_number"] = run_num
run_info_dict["start_time"] = current_time
run_info_dict["sample_rate_GSa/s"] = sample_rate
run_info_dict["ms_per_div"] = horizontal_scale
run_info_dict["memory_depth_MSa"] = memory_depth
run_info_dict["active_channels"] = active_channels
run_info_dict["scale_ch1"] = scale_ch1
run_info_dict["scale_ch2"] = scale_ch2
run_info_dict["scale_ch3"] = scale_ch3
run_info_dict["scale_ch4"] = scale_ch4
run_info_dict["trigger_channel"] = trigger_channel
run_info_dict["trigger_threshold"] = trigger_threshold
run_info_dict["horizontal_delay"] = horizontal_delay

if saveData:
    with open("run_log.txt", 'a') as run_log:
        run_log.write(json.dumps(run_info_dict))
        run_log.write(",\n")
    with open("unpack_commands.txt",'a') as unpack_commands:
        command = ".\\bintest.exe {}".format(filename)
        for chan in active_channels:
            command += " {}".format(filename.replace(".bin","_{}.csv".format(chan)).replace("bin","csv") )
        command +="\n"
        # print(command)
        unpack_commands.write(command)
 
    ### this doesn't seem to detect failures actually.
    rigol.query(":SAVE:STATus?") ### this seems to stop python flow until saving is complete. OR NOT.
    for i in range(10):
        time.sleep(1)
        save_status = int(rigol.query(":SAVE:STATus?"))
        # print(save_status)

        if save_status==1: 
            print("Save successful.")
            break
    if save_status==0:
        print("SAVE FAILURE.")
rigol.close()

# end = datetime.datetime.now()
final = time.time()
# current_time = end.strftime("%H:%M:%S")
print("\n\n ######### Finished Run {}. Elapsed time {:.1f} s ########".format(run_num,final-initial))
print("----------------------------------------------------------------------\n\n")
# print("Elapsed time: {}".format((end-now).str`("%M:%S")))