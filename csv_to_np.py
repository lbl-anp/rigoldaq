import numpy as np
import matplotlib.pyplot as plt
import argparse
import glob
import pickle

parser = argparse.ArgumentParser()
parser.add_argument('--run', type=int, default = 0, required=True)
# parser.add_argument('--path',type=str,default="out",required=False)

args = parser.parse_args()
run = int(args.run)

file_list = sorted(glob.glob("data/csv/wave{}*.csv".format(run)))
print("Converting Run {}.".format(run))
print("Found {} channels, in files {}.".format(len(file_list),file_list)) 

waves = []
for i,file in enumerate(file_list):
    if i==1 or i==2 or i==3: continue
    channel = np.genfromtxt(file, delimiter=',')
    channel[:,0]*=1000.
    channel[:,1]*=1000.
    waves.append(channel)
    

# print(test[0])
# print(test[1][0]-test[0][0])
# print(test[1])
# print(test[2])



disp, ax = plt.subplots(1,1,figsize=(8,4.7), dpi=150,tight_layout = True)

ax.plot(waves[0][:,0],waves[0][:,1], 'orange', marker = ".", alpha = 1,markersize =1 )
# ax.plot(waves[1][:,0],waves[1][:,1], 'blue', marker = ".", alpha = 0.5,markersize =1 )
# plt.title("Event {}".format(n))
ax.set_xlabel('Time [ms]')
ax.set_ylabel('Voltage [mV]')
disp.savefig("displays/event{}.png".format(run))


plot_range_low = 1000000
plot_range_high = 1001000

disp2, ax2 = plt.subplots(1,1,figsize=(8,4.7), dpi=150,tight_layout = True)

ax2.plot(waves[0][plot_range_low:plot_range_high,0],waves[0][plot_range_low:plot_range_high,1], 'orange', marker = ".", alpha = 1,markersize =1 )
# ax.plot(waves[1][:,0],waves[1][:,1], 'blue', marker = ".", alpha = 0.5,markersize =1 )
# plt.title("Event {}".format(n))
ax2.set_xlabel('Time [ms]')
ax2.set_ylabel('Voltage [mV]')
disp2.savefig("displays/event{}_zoom.png".format(run))




with open('data/np/wave{}.pickle'.format(run), 'wb') as handle:
    pickle.dump(waves, handle, protocol=pickle.HIGHEST_PROTOCOL)

### read like this
# with open(input_filename, 'rb') as handle:
    # [processed_clusters,flat_table] = pickle.load(handle)